# CalAssig

_A library that allows you to calculate the days worked._

## Features

- Calculate the number of workdays between two given dates
- Add holidays and other non-working days
- Import holidays from JSON file
- Export holidays to JSON file

## Tech

The application it was development in java programming language and maven for the construccion.

## Install

### Javadoc
```java
mvn javadoc:javadoc
```

### Testing
```java
mvn test
```

### Build Jar file from source
```java
mvn install
```

## How to Use
```java
package org.example;

import com.company.calendar.CalendarAssignment;
import com.company.exceptions.DatesException;

import java.time.LocalDate;

public class App {
  public static void main( String[] args ) throws DatesException {
    CalendarAssignment calAssig = new CalendarAssignment();
    LocalDate startLocalDate = LocalDate.of(2022, 06, 27);
    LocalDate endLocalDate = LocalDate.of(2022, 07, 04);
    int result = calAssig.getNumberOfWorkDays(startLocalDate, endLocalDate);
    System.out.println(result);
  }
}
```
