package com.company.calendar;

import com.company.exceptions.DatesException;
import com.company.exceptions.IOCalendarException;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;

import static org.junit.Assert.*;

public class CalendarAssignmentTest {

  CalendarAssignment calAssig;
  String resourcePath;

  @Before
  public void setUp() {
    calAssig = new CalendarAssignment();
    Path resourceDirectory = Paths.get("src","test","resources");
    resourcePath = resourceDirectory.toFile().getAbsolutePath();
  }

  @Test
  public void return_6_between_June_27_to_July_4() throws DatesException {
    LocalDate startLocalDate = LocalDate.of(2022, 06, 27);
    LocalDate endLocalDate = LocalDate.of(2022, 07, 04);
    int result = calAssig.getNumberOfWorkDays(startLocalDate, endLocalDate);
    assertEquals(6, result);
  }

  @Test
  public void return_5_between_June_27_to_July_4_with_a_holiday() throws DatesException {
    LocalDate holidayDate = LocalDate.of(2022, 07, 01);
    calAssig.addHoliday(holidayDate);
    LocalDate startLocalDate = LocalDate.of(2022, 06, 27);
    LocalDate endLocalDate = LocalDate.of(2022, 07, 04);
    int result = calAssig.getNumberOfWorkDays(startLocalDate, endLocalDate);
    assertEquals(5, result);
  }

  @Test
  public void return_4_between_June_27_to_July_4_with_two_holiday() throws DatesException {
    LocalDate holidayDate1 = LocalDate.of(2022, 06, 30);
    calAssig.addHoliday(holidayDate1);
    LocalDate holidayDate2 = LocalDate.of(2022, 07, 01);
    calAssig.addHoliday(holidayDate2);
    LocalDate startLocalDate = LocalDate.of(2022, 06, 27);
    LocalDate endLocalDate = LocalDate.of(2022, 07, 04);
    int result = calAssig.getNumberOfWorkDays(startLocalDate, endLocalDate);
    assertEquals(4, result);
  }

  @Test
  public void return_1_when_the_dates_are_the_same_in_work_days() throws DatesException {
    LocalDate startLocalDate = LocalDate.of(2022, 06, 30);
    LocalDate endLocalDate = LocalDate.of(2022, 06, 30);
    int result = calAssig.getNumberOfWorkDays(startLocalDate, endLocalDate);
    assertEquals(1, result);
  }

  @Test
  public void return_3_holidays_from_json_file() throws IOCalendarException {
    String fileName = resourcePath + "/dates.json";
    File file = new File(fileName);
    int holdaysAdded = calAssig.readHolidays(file);
    assertEquals(3, holdaysAdded);
  }

  @Test(expected = IOCalendarException.class)
  public void return_3_holidays_from_not_exist_json_file() throws IOCalendarException {
    String fileName = resourcePath + "/not_exists.json";
    File file = new File(fileName);
    int holdaysAdded = calAssig.readHolidays(file);
    assertEquals(3, holdaysAdded);
  }

  @Test
  public void return_2_holidays_in_a_json_file() throws IOCalendarException {
    LocalDate holidayDate1 = LocalDate.of(2022, 06, 30);
    calAssig.addHoliday(holidayDate1);
    LocalDate holidayDate2 = LocalDate.of(2022, 07, 01);
    calAssig.addHoliday(holidayDate2);
    String fileName = resourcePath + "/dates2.json";
    File file = new File(fileName);
    calAssig.writeHolidays(file);
    int holdaysAdded = calAssig.readHolidays(file);
    assertEquals(2, holdaysAdded);
  }

  @Test(expected = DatesException.class)
  public void return_exception_when_startDate_is_greater_than_endDate() throws DatesException {
    LocalDate startLocalDate = LocalDate.of(2022, 07, 04);
    LocalDate endLocalDate = LocalDate.of(2022, 06, 27);
    calAssig.getNumberOfWorkDays(startLocalDate, endLocalDate);
  }

}