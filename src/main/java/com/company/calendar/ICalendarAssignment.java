package com.company.calendar;

import com.company.exceptions.DatesException;
import com.company.exceptions.IOCalendarException;

import java.io.File;
import java.time.LocalDate;

public interface ICalendarAssignment {

  /**
   * @param startLocalDate start date of the range to evaluate
   * @param endLocalDate end date of the range to evaluate
   * @return number of days worked without weekends and holidays
   * @throws DatesException
   */
  public int getNumberOfWorkDays(LocalDate startLocalDate, LocalDate endLocalDate) throws DatesException;

  /**
   * @param localDate date to add in holiday list
   */
  public void addHoliday(LocalDate localDate);

  /**
   * @param file to read the holidays in json format
   * @return number of imported holidays
   * @throws IOCalendarException
   */
  public int readHolidays(File file) throws IOCalendarException;

  /**
   * @param file to write the holidays in json format
   * @throws IOCalendarException
   */
  public void writeHolidays(File file) throws IOCalendarException;

}
