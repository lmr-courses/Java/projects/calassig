package com.company.calendar;

import com.company.exceptions.DatesException;
import com.company.exceptions.IOCalendarException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static java.time.DayOfWeek.*;

public class CalendarAssignment implements ICalendarAssignment{
  private static final Logger logger = Logger.getLogger(CalendarAssignment.class.getName());
  private static final String DEFAULT_FORMAT_DATE = "yyyy-MM-dd";
  private Set<LocalDate> holidays = new HashSet<>();
  private final Set<DayOfWeek> workDays = Set.of( MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY);
  private ObjectMapper mapper = new ObjectMapper();
  private DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DEFAULT_FORMAT_DATE);


  @Override
  public int getNumberOfWorkDays(LocalDate startLocalDate, LocalDate endLocalDate) throws DatesException {
    if(startLocalDate.isAfter(endLocalDate)) throw new DatesException("The startLocalDate cannot be greater than endLocalDate");
    endLocalDate = endLocalDate.plusDays(1);//inclusive
    List<LocalDate> allDates = startLocalDate.datesUntil(endLocalDate)
            .filter(t -> workDays.contains(t.getDayOfWeek()))
            .filter(t -> !holidays.contains(t))
            .collect(Collectors.toList());
    return allDates.size();
  }

  @Override
  public void addHoliday(LocalDate localDate) {
    holidays.add(localDate);
    logger.log(Level.INFO, "Holiday added: {0}", localDate);
  }

  @Override
  public int readHolidays(File file) throws IOCalendarException {
    int result = 0;
    try {
      List<String> values = mapper.readValue(file, new TypeReference<>() {});
      values.forEach(s -> {
        LocalDate localDate = LocalDate.parse(s, formatter);
        addHoliday(localDate);
      });
      logger.log(Level.INFO, "{0} Holidays added.", values.size());
      result = values.size();
    } catch (IOException e){
      throw new IOCalendarException("The file cannot be read", e);
    }
    return result;
  }

  @Override
  public void writeHolidays(File file) throws IOCalendarException {
    try (FileWriter fileWriter = new FileWriter(file)) {
      List<String> export = holidays.stream().map(e -> e.format(formatter)).collect(Collectors.toList());
      String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(export);
      fileWriter.write(json);
      fileWriter.flush();
      logger.log(Level.INFO, "{0} written holidays.", export.size());
    } catch (IOException e) {
      throw new IOCalendarException("The file cannot be write", e);
    }
  }

}
