package com.company.exceptions;


public class IOCalendarException extends Exception {
  public IOCalendarException(String message, Throwable cause) {
    super(message, cause);
  }
}
