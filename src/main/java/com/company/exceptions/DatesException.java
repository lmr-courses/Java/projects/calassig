package com.company.exceptions;

public class DatesException extends Exception{
  public DatesException(String message) {
    super(message);
  }
}
